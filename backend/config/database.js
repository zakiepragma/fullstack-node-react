import { Sequelize } from "sequelize";

const db = new Sequelize("fullstack", "root", "password", {
  host: "localhost",
  dialect: "mysql",
});

export default db;
