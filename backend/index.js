import express from "express";
import db from "./config/database.js";
import productRoutes from "./routes/index.js";

const app = express();

try {
  await db.authenticate();
  console.log("Database terkoneksi");
} catch (error) {
  console.error("Koneksi error:", error);
}
//jika / sebagai route utama
// app.use(productRoutes);

//jika /products sebagai route utama
app.use("/products", productRoutes);

app.listen(5000, () => console.log("Server berjalan di port 5000"));
